/**/

export interface ClearChargingProfileRequest {
  id?: number;
  connectorId?: number;
  chargingProfilePurpose?: ChargingProfilePurpose;
  stackLevel?: number;
}

export enum ChargingProfilePurpose {
  ChargePointMaxProfile = "ChargePointMaxProfile",
  TxDefaultProfile = "TxDefaultProfile",
  TxProfile = "TxProfile"
}
