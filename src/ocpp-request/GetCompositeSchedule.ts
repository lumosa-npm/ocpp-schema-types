/**/

export interface GetCompositeScheduleRequest {
  connectorId: number;
  duration: number;
  chargingRateUnit?: ChargingRateUnit;
}

export enum ChargingRateUnit {
  A = "A",
  W = "W"
}
