/**/

export interface ChangeConfigurationRequest {
  key: string;
  value: string;
}
