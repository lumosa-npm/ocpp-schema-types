/**/

export interface DiagnosticsStatusNotificationRequest {
  status: DiagnosticsStatusNotificationStatus;
}

export enum DiagnosticsStatusNotificationStatus {
  Idle = "Idle",
  Uploaded = "Uploaded",
  UploadFailed = "UploadFailed",
  Uploading = "Uploading"
}
