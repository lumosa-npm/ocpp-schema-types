/**/

export interface TriggerMessageRequest {
  requestedMessage: RequestedTriggerMessage;
  connectorId?: number;
}

export enum RequestedTriggerMessage {
  BootNotification = "BootNotification",
  DiagnosticsStatusNotification = "DiagnosticsStatusNotification",
  FirmwareStatusNotification = "FirmwareStatusNotification",
  Heartbeat = "Heartbeat",
  MeterValues = "MeterValues",
  StatusNotification = "StatusNotification"
}
