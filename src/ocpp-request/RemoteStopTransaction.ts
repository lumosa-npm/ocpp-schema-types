/**/

export interface RemoteStopTransactionRequest {
  transactionId: number;
}
