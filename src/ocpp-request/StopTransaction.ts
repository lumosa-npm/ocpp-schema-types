/**/

export interface StopTransactionRequest {
  idTag?: string;
  meterStop: number;
  timestamp: string;
  transactionId: number;
  reason?: StopTransactionReason;
  transactionData?: {
    timestamp: string;
    sampledValue: {
      value: string;
      context?:
        | "Interruption.Begin"
        | "Interruption.End"
        | "Sample.Clock"
        | "Sample.Periodic"
        | "Transaction.Begin"
        | "Transaction.End"
        | "Trigger"
        | "Other";
      format?: "Raw" | "SignedData";
      measurand?:
        | "Energy.Active.Export.Register"
        | "Energy.Active.Import.Register"
        | "Energy.Reactive.Export.Register"
        | "Energy.Reactive.Import.Register"
        | "Energy.Active.Export.Interval"
        | "Energy.Active.Import.Interval"
        | "Energy.Reactive.Export.Interval"
        | "Energy.Reactive.Import.Interval"
        | "Power.Active.Export"
        | "Power.Active.Import"
        | "Power.Offered"
        | "Power.Reactive.Export"
        | "Power.Reactive.Import"
        | "Power.Factor"
        | "Current.Import"
        | "Current.Export"
        | "Current.Offered"
        | "Voltage"
        | "Frequency"
        | "Temperature"
        | "SoC"
        | "RPM";
      phase?: "L1" | "L2" | "L3" | "N" | "L1-N" | "L2-N" | "L3-N" | "L1-L2" | "L2-L3" | "L3-L1";
      location?: "Cable" | "EV" | "Inlet" | "Outlet" | "Body";
      unit?:
        | "Wh"
        | "kWh"
        | "varh"
        | "kvarh"
        | "W"
        | "kW"
        | "VA"
        | "kVA"
        | "var"
        | "kvar"
        | "A"
        | "V"
        | "K"
        | "Celcius"
        | "Celsius"
        | "Fahrenheit"
        | "Percent";
      [k: string]: unknown;
    }[];
    [k: string]: unknown;
  }[];
}

export enum StopTransactionReason {
  EmergencyStop = "EmergencyStop",
  EVDisconnected = "EVDisconnected",
  HardReset = "HardReset",
  Local = "Local",
  Other = "Other",
  PowerLoss = "PowerLoss",
  Reboot = "Reboot",
  Remote = "Remote",
  SoftReset = "SoftReset",
  UnlockCommand = "UnlockCommand",
  DeAuthorized = "DeAuthorized"
}
