/**/

export interface ChangeAvailabilityRequest {
  connectorId: number;
  type: ChangeAvailabilityType;
}

export enum ChangeAvailabilityType {
  Inoperative = "Inoperative",
  Operative = "Operative"
}
