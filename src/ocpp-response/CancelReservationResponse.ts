/**/

export interface CancelReservationResponse {
  status: CancelReservationStatus;
}

export enum CancelReservationStatus {
  Accepted = "Accepted",
  Rejected = "Rejected"
}
