/**/

export interface SetChargingProfileResponse {
  status: SetChargingProfileStatus;
}

export enum SetChargingProfileStatus {
  Accepted = "Accepted",
  Rejected = "Rejected",
  NotSupported = "NotSupported"
}
