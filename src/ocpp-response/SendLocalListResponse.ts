/**/

export interface SendLocalListResponse {
  status: SendLocalListStatus;
}

export enum SendLocalListStatus {
  Accepted = "Accepted",
  Failed = "Failed",
  NotSupported = "NotSupported",
  VersionMismatch = "VersionMismatch"
}
