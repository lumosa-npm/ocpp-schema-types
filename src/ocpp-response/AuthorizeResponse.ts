/**/

export interface AuthorizeResponse {
  idTagInfo: {
    status: IdTagInfoStatus;
    expiryDate?: string;
    parentIdTag?: string;
    [k: string]: unknown;
  };
}

export enum IdTagInfoStatus {
  Accepted = "Accepted",
  Blocked = "Blocked",
  Expired = "Expired",
  Invalid = "Invalid",
  ConcurrentTx = "ConcurrentTx"
}
