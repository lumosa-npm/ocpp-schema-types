/**/

export interface ClearChargingProfileResponse {
  status: ClearChargingProfileStatus;
}

export enum ClearChargingProfileStatus {
  Accepted = "Accepted",
  Unknown = "Unknown"
}
