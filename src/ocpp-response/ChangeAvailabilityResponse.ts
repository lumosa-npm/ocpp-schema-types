/**/

export interface ChangeAvailabilityResponse {
  status: ChangeAvailabilityStatus;
}

export enum ChangeAvailabilityStatus {
  Accepted = "Accepted",
  Rejected = "Rejected",
  Scheduled = "Scheduled"
}
