/**/

export interface RemoteStartTransactionResponse {
  status: RemoteStartTransactionStatus;
}

export enum RemoteStartTransactionStatus {
  Accepted = "Accepted",
  Rejected = "Rejected"
}
