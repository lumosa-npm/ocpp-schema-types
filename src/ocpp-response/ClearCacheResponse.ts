/**/

export interface ClearCacheResponse {
  status: ClearCacheStatus;
}

export enum ClearCacheStatus {
  Accepted = "Accepted",
  Rejected = "Rejected"
}
