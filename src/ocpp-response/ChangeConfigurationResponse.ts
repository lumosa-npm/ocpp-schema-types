/**/

export interface ChangeConfigurationResponse {
  status: ChangeConfigurationStatus;
}

export enum ChangeConfigurationStatus {
  Accepted = "Accepted",
  Rejected = "Rejected",
  RebootRequired = "RebootRequired",
  NotSupported = "NotSupported"
}
