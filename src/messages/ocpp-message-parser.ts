import { OcppCall } from "./ocpp-call";
import { OcppCallError } from "./ocpp-call-error";
import { OcppCallResult } from "./ocpp-call-result";
import { OcppMessageType } from "./ocpp-message-type";

export class OcppMessageParser {
    static parse(incomingData: string): OcppCall<unknown> | OcppCallResult<unknown> | OcppCallError { //AbstractOcppMessage {
        try {
            const data: any[] = JSON.parse(incomingData);
            if (!Array.isArray) {
                throw 'ocpp message must be an array';
            }

            const [type, ocppRequestId, ...rest] = data;
            if (typeof ocppRequestId !== 'string') {
                throw 'ocpp message id must be a string';
            }

            switch (type) {
                /* OCPP-CALL format [2, id, command, payload] */
                case OcppMessageType.Request: {
                    if (data.length != 4) {
                        throw `ocpp call message must be an array with 4 items: now is ${data.length}`;
                    }
                    const [command, payload] = rest;
                    if (typeof payload !== 'object') {
                        throw `ocpp call payload attribute must be an object: now is ${payload}`;
                    }
                    console.log(`ocpp call received:\n${incomingData}`);
                    return new OcppCall<unknown>({
                        command,
                        payload,
                        id: ocppRequestId
                    });
                }

                /* OCPP-CALLRESULT format [3, id, payload] */
                case OcppMessageType.Result: {
                    if (data.length != 3) {
                        throw `ocpp call result message must be an array with 3 items: now is ${data.length}`;
                    }
                    const [payload] = rest;
                    console.log(`ocpp call result:\n${incomingData}`);
                    return new OcppCallResult({
                        id: ocppRequestId,
                        payload
                    });
                }

                /* OCPP-CALLERROR format [4, id, code, message, description] */
                case OcppMessageType.Error: {
                    if (data.length != 5) {
                        throw `ocpp call error message must be an array with 5 items: now is ${data.length}`;
                    }
                    const [code, message, description] = rest;
                    if (typeof message !== 'string') {
                        throw `message attribute must be a string: now is ${message}`;
                    }
                    console.log(`ocpp call error:\n${incomingData}`);
                    return new OcppCallError({
                        id: ocppRequestId,
                        details: description,
                        code,
                        message
                    });
                }

                default: {
                    throw `ocpp message type must be 2, 3 or 4: now is ${type}`;
                }
            }
        } catch (error) {
            console.warn(`Invalid ocpp message received ${incomingData}:\n${JSON.stringify(error)}`);
            return null;
        }
    }
}