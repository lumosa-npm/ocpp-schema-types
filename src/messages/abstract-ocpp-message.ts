import { OcppMessageType } from "./ocpp-message-type";

export abstract class AbstractOcppMessage {
  id: string;

  constructor(id: string) {
    this.id = id;
  }

  /**
   * stringifies the ocpp json object value
   */
  toString(): string {
    return JSON.stringify(this.toJson());
  }

  /**
   * Return the type of ocpp message the subclass implements
   */
  abstract type(): OcppMessageType;

  /**
   * converts class attributes to required ocpp json object in proper ocpp format
   */
  abstract toJson(): unknown[];
}
