import { OcppMessageType } from './ocpp-message-type';
import { AbstractOcppMessage } from './abstract-ocpp-message';

export class OcppCallResult<Tpayload> extends AbstractOcppMessage {
  payload: Tpayload;

  constructor(data: {
    id: string, 
    payload: Tpayload
  }) {
    super(data.id);
    this.payload = data.payload;
  }

  type(): OcppMessageType {
    return OcppMessageType.Result;
  }

  toJson(): [OcppMessageType, string, Tpayload] {
    return [this.type(), this.id, this.payload];
  }
}
