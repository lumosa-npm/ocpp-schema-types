import { OcppErrorCode, OcppErrorMessage } from '../enums/ocpp-errors';
import { OcppMessageType } from './ocpp-message-type';
import { AbstractOcppMessage } from './abstract-ocpp-message';

export class OcppCallError extends AbstractOcppMessage {
  code: OcppErrorCode;
  message: OcppErrorMessage | string;
  details: unknown;

  constructor(data: {
    id: string, 
    details?: unknown,
    code?: OcppErrorCode,
    message?: OcppErrorMessage | string
  }) {
    super(data.id);
    this.code = data?.code ?? OcppErrorCode.GenericError;
    this.message = data?.message ?? OcppErrorMessage.GenericError;
    this.details = data?.details ?? {}
  }

  type(): OcppMessageType {
    return OcppMessageType.Error;
  }

  toJson(): [OcppMessageType, string, OcppErrorCode, OcppErrorMessage | string, unknown] {
    return [this.type(), this.id, this.code, this.message, this.details];
  }
}
