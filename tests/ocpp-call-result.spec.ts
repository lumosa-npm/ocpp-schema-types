import { OcppCallResult, OcppMessageType } from '../src/messages/index';

describe('ocpp call result', () => {
  it('must be the correct type', () => {
    const v = new OcppCallResult({
      id: "0",
      payload: {}
    });
    expect(v.type()).toBe(OcppMessageType.Result);
  });

  it('should return empty payload if not provided', () => {
    const v = new OcppCallResult({
      id: "0",
      payload: {}
    });
    expect(v.payload).toEqual({});
  });

  it('must return correct payload if provided', () => {
    const payload = { a: 'dummy', b: 4, c: true, d: {}, e: [1, 2, 3] };
    const v = new OcppCallResult({
      id: "3214325",
      payload
    });
    expect(v.payload).toEqual(payload);
  });

  it('must return correct ocpp result json', () => {
    const id = '1213224';
    const payload = { a: 'dummy' };
    const v = new OcppCallResult({
      id,
      payload
    });
    expect(v.toJson()).toEqual([OcppMessageType.Result, id, payload]);
  });

  it('must return correct stringified data', () => {
    const id = '1213224';
    const payload = { a: 'dummy' };
    const v = new OcppCallResult({
      id,
      payload
    });
    expect(v.toString()).toBe(JSON.stringify([OcppMessageType.Result, id, payload]));
  });

});
